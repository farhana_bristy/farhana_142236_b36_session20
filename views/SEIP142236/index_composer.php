<?php
require_once("../../vendor/autoload.php");
use App\Person;
use App\Student;


$objPerson = new Person();
$objStudent = new Student();

$objPerson->showPersonInfo();
$objStudent->showStudentInfo();
?>